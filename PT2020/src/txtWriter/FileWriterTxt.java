package txtWriter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriterTxt {
	private static int index = 1;
	private final static String ENDL = "\n";
	private final static String PATH = "C:\\Users\\Pc\\eclipse-workspace\\Project5TP\\file_";
	
	public static void fileWriterTxt(String writeData) {	
		String[] content = writeData.split(ENDL);
		try (FileWriter file = new FileWriter(PATH + index + ".txt"); BufferedWriter buff = new BufferedWriter(file)) {
			int i = 0;
			while (i < content.length) {
				buff.write(content[i]);
				buff.newLine();
				i += 1;
			}
			index++;
		} catch (IOException e) {
			System.out.println("Nu s-a putut crea fisierul .txt!");
		}

	}
}
