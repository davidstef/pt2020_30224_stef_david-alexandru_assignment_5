package main;

import infoData.ActivityData;

public class MainClass {
	
	public static void main(String args[]) {
		ActivityData activity = new ActivityData("Activities.txt");
		activity.citesteDinFisier();
		activity.numaraZilele();
		activity.numaraActivitati();
		activity.numaraActivitatiPeZile();
		activity.afiseazaIntreagaDurata();
		activity.filterActivities();
	}
}
